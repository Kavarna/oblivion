#include "Oblivion.h"
#include "Direct3D.h"
#include <dxgidebug.h>

Direct3D::Direct3D( bool useWARP )
{
#if DEBUG || _DEBUG
    EnableDebug( );
#endif

    m_d3d12Device = CreateD3D12Device( GetAdapter( useWARP ) );
    m_tearingSupport = CheckTearingSupport( );

    m_directCommandQueue = std::make_shared<CommandQueue>( m_d3d12Device, D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_DIRECT );
    m_copyCommandQueue = std::make_shared<CommandQueue>( m_d3d12Device, D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_COPY );
    m_computeCommandQueue = std::make_shared<CommandQueue>( m_d3d12Device, D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_COMPUTE );
}

Direct3D::~Direct3D( )
{    
}

ComPtr<ID3D12DescriptorHeap> Direct3D::CreateDescriptorHeap( UINT numDescriptors,
                                                             D3D12_DESCRIPTOR_HEAP_TYPE type,
                                                             D3D12_DESCRIPTOR_HEAP_FLAGS flags )
{
    D3D12_DESCRIPTOR_HEAP_DESC descriptorHeapDesc;
    descriptorHeapDesc.NodeMask = 0;
    descriptorHeapDesc.Flags = flags;
    descriptorHeapDesc.Type = type;
    descriptorHeapDesc.NumDescriptors = numDescriptors;

    ComPtr<ID3D12DescriptorHeap> descriptorHeap;
    ThrowIfFailed(
        m_d3d12Device->CreateDescriptorHeap(
            &descriptorHeapDesc,
            IID_PPV_ARGS( &descriptorHeap )
        )
    );

    return descriptorHeap;
}

UINT Direct3D::GetDescriptorIncreaseSize( D3D12_DESCRIPTOR_HEAP_TYPE type )
{
    return m_d3d12Device->GetDescriptorHandleIncrementSize( type );
}

bool Direct3D::HasTearingSupport( )
{
    return m_tearingSupport;
}

ComPtr<ID3D12Device6> Direct3D::GetD3D12Device( )
{
    return m_d3d12Device;
}

void Direct3D::FlushAll()
{
    m_copyCommandQueue->Flush( );
    m_directCommandQueue->Flush( );
    m_computeCommandQueue->Flush( );
}

void Direct3D::EnableDebug( )
{
    ComPtr<ID3D12Debug> debugInterface;
    D3D12GetDebugInterface( IID_PPV_ARGS( &debugInterface ) );
    debugInterface->EnableDebugLayer( );
}


ComPtr<IDXGIAdapter4> Direct3D::GetAdapter( bool useWARP )
{
    ComPtr<IDXGIAdapter4> adapter4;
    ComPtr<IDXGIFactory5> factory5;
    UINT flags = 0;
#if DEBUG || _DEBUG
    flags = DXGI_CREATE_FACTORY_DEBUG;
#endif
    ThrowIfFailed(
        CreateDXGIFactory2( flags, IID_PPV_ARGS( &factory5 ) )
    );

    if ( useWARP )
    {
        ThrowIfFailed(
            factory5->EnumWarpAdapter( IID_PPV_ARGS( &adapter4 ) )
        );
    }
    else
    {
        ComPtr<IDXGIAdapter> adapter;
        SIZE_T maxDedicatedVideoMemory = 0;
        for ( UINT i = 0; factory5->EnumAdapters( i, &adapter ) != DXGI_ERROR_NOT_FOUND; ++i )
        {
            ComPtr<IDXGIAdapter4> tempAdapter4;
            DXGI_ADAPTER_DESC3 desc;
            adapter.As( &tempAdapter4 );
            tempAdapter4->GetDesc3( &desc );
            if ( (desc.Flags & DXGI_ADAPTER_FLAG::DXGI_ADAPTER_FLAG_SOFTWARE) == 0 &&
                 SUCCEEDED( D3D12CreateDevice( tempAdapter4.Get( ),
                                               D3D_FEATURE_LEVEL::D3D_FEATURE_LEVEL_12_1,
                                               __uuidof(ID3D12Device6), nullptr ) ) &&
                 desc.DedicatedVideoMemory > maxDedicatedVideoMemory )
            {
                maxDedicatedVideoMemory = desc.DedicatedVideoMemory;
                adapter4 = tempAdapter4;
            }
        }
    }

    return adapter4;
}

ComPtr<ID3D12Device6> Direct3D::CreateD3D12Device( ComPtr<IDXGIAdapter4> adapter )
{
    ComPtr<ID3D12Device6> device;
    ThrowIfFailed(
        D3D12CreateDevice(
            adapter.Get( ),
            D3D_FEATURE_LEVEL::D3D_FEATURE_LEVEL_12_1,
            IID_PPV_ARGS( &device )
        )
    );

#if DEBUG || _DEBUG
    ComPtr<ID3D12InfoQueue> infoQueue;
    if ( SUCCEEDED( device.As( &infoQueue ) ) )
    {
        infoQueue->SetBreakOnSeverity( D3D12_MESSAGE_SEVERITY::D3D12_MESSAGE_SEVERITY_ERROR, true );
        infoQueue->SetBreakOnSeverity( D3D12_MESSAGE_SEVERITY::D3D12_MESSAGE_SEVERITY_WARNING, true );
        infoQueue->SetBreakOnSeverity( D3D12_MESSAGE_SEVERITY::D3D12_MESSAGE_SEVERITY_CORRUPTION, true );
    }
#endif

    return device;
}

bool Direct3D::CheckTearingSupport( )
{
    ComPtr<IDXGIFactory5> factory5;
    ThrowIfFailed(
        CreateDXGIFactory1( IID_PPV_ARGS( &factory5 ) )
    );

    BOOL hasSupport;
    ThrowIfFailed(
        factory5->CheckFeatureSupport( DXGI_FEATURE::DXGI_FEATURE_PRESENT_ALLOW_TEARING,
                                       &hasSupport, sizeof( hasSupport ) )
    );
    
    return hasSupport;
}

