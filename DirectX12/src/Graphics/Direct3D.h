#pragma once


#include "Oblivion.h"
#include "CommandQueue.h"


class Direct3D: public ISingletone<Direct3D>
{
public:
    Direct3D( bool useWARP );
    ~Direct3D( );

public:
    ComPtr<ID3D12DescriptorHeap> CreateDescriptorHeap( UINT numDescriptors,
                                                       D3D12_DESCRIPTOR_HEAP_TYPE type,
                                                       D3D12_DESCRIPTOR_HEAP_FLAGS flags = D3D12_DESCRIPTOR_HEAP_FLAGS::D3D12_DESCRIPTOR_HEAP_FLAG_NONE );
    UINT GetDescriptorIncreaseSize( D3D12_DESCRIPTOR_HEAP_TYPE type );

    bool HasTearingSupport( );
    ComPtr<ID3D12Device6> GetD3D12Device( );

    template< D3D12_COMMAND_LIST_TYPE type>
    std::shared_ptr<CommandQueue> GetCommandQueue( )
    {
        if constexpr ( type == D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_DIRECT )
        {
            return m_directCommandQueue;
        }
        else if constexpr ( type == D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_COPY )
        {
            return m_copyCommandQueue;
        }
        else if constexpr ( type == D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_COMPUTE )
        {
            return m_computeCommandQueue;
        }
        else
        {
            static_assert(false,
                           "Template argument for GetCommandQueue is not valid");
        }
    }

public:
    void FlushAll( );

private:
    void EnableDebug( );

    ComPtr<IDXGIAdapter4> GetAdapter( bool useWARP );
    ComPtr<ID3D12Device6> CreateD3D12Device( ComPtr<IDXGIAdapter4> adapter );
    bool CheckTearingSupport( );


private:
    ComPtr<ID3D12Device6> m_d3d12Device;

    std::shared_ptr<CommandQueue> m_directCommandQueue;
    std::shared_ptr<CommandQueue> m_copyCommandQueue;
    std::shared_ptr<CommandQueue> m_computeCommandQueue;

    bool m_tearingSupport;
    
};

