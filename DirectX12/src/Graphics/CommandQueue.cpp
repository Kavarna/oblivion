#include "Oblivion.h"
#include "CommandQueue.h"

CommandQueue::CommandQueue( ComPtr<ID3D12Device6> device, D3D12_COMMAND_LIST_TYPE type ):
    m_d3d12Device( device ),
    m_commandListType( type ),
    m_fenceValue( 0 )
{
    D3D12_COMMAND_QUEUE_DESC commandQueueDesc = {};
    commandQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAGS::D3D12_COMMAND_QUEUE_FLAG_NONE;
    commandQueueDesc.NodeMask = 0;
    commandQueueDesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY::D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
    commandQueueDesc.Type = type;

    ThrowIfFailed(
        device->CreateCommandQueue(
            &commandQueueDesc,
            IID_PPV_ARGS( &m_d3d12CommandQueue )
        )
    );

    ThrowIfFailed(
        device->CreateFence(
            0,
            D3D12_FENCE_FLAGS::D3D12_FENCE_FLAG_NONE,
            IID_PPV_ARGS( &m_fence )
        )
    );

    m_fenceEvent = CreateEvent( NULL, NULL, NULL, NULL );
    EVALUATE( m_fence, NULL, == , "Unable to create fence event." );
}

CommandQueue::~CommandQueue( )
{ }



ComPtr<ID3D12GraphicsCommandList> CommandQueue::GetCommandList( ) const
{
    ComPtr<ID3D12CommandAllocator> commandAllocator;
    ComPtr<ID3D12GraphicsCommandList> commandList;

    if ( !m_queueAllocatorFence.empty( ) &&
         IsFenceCompleted( m_queueAllocatorFence.front( ).fenceValue ) )
    { // Try get command allocator that is free
        auto element = m_queueAllocatorFence.front( );
        m_queueAllocatorFence.pop( );
        commandAllocator = element.allocator;
    }
    else
    {
        commandAllocator = CreateCommandAllocator( );
    }
    ThrowIfFailed( commandAllocator->Reset( ) );

    if ( !m_queueCommandLists.empty( ) )
    {
        commandList = m_queueCommandLists.front( );
        m_queueCommandLists.pop( );
        ThrowIfFailed( commandList->Reset( commandAllocator.Get( ), nullptr ) );
    }
    else
    {
        commandList = CreateGraphicsCommandList( commandAllocator );
    }


    ThrowIfFailed( commandList->SetPrivateDataInterface( __uuidof(ID3D12CommandAllocator), commandAllocator.Get() ) );

    return commandList;

}

UINT64 CommandQueue::ExecuteCommandList( ComPtr<ID3D12GraphicsCommandList> commandList ) const
{
    ThrowIfFailed( commandList->Close( ) );

    ID3D12CommandAllocator* allocator;
    UINT allocatorSize = sizeof( allocator );
    commandList->GetPrivateData( __uuidof(ID3D12CommandAllocator), &allocatorSize, &allocator );

    ID3D12CommandList* commandLists[ ]
    {
        commandList.Get( )
    };

    m_d3d12CommandQueue->ExecuteCommandLists( 1, commandLists );
    UINT64 fenceValue = Signal( );

    // Put back the toys
    m_queueAllocatorFence.emplace( AllocatorFence { allocator, fenceValue } );
    m_queueCommandLists.push( commandList );
  
    // Handled by m_queueAllocatorFence next
    allocator->Release( );

    return fenceValue;
}

ID3D12CommandQueue* CommandQueue::GetD3D12CommandQueue( ) const
{
    return m_d3d12CommandQueue.Get( );
}

void CommandQueue::Flush( ) const
{
    WaitForFenceValue( Signal( ) );
}

UINT64 CommandQueue::Signal( ) const
{
    ThrowIfFailed(
        m_d3d12CommandQueue->Signal( m_fence.Get( ), ++m_fenceValue )
    );

    return m_fenceValue;
}

bool CommandQueue::IsFenceCompleted( UINT64 fenceValue ) const
{
    return m_fence->GetCompletedValue( ) >= fenceValue;
}

void CommandQueue::WaitForFenceValue( UINT64 fenceValue ) const
{
    if ( !IsFenceCompleted( fenceValue ) )
    {
        m_fence->SetEventOnCompletion( fenceValue, m_fenceEvent );
        WaitForSingleObject( m_fenceEvent, std::numeric_limits<DWORD>::max( ) );
    }
}

ComPtr<ID3D12CommandAllocator> CommandQueue::CreateCommandAllocator( ) const
{
    ComPtr<ID3D12CommandAllocator> allocator;
    ThrowIfFailed( m_d3d12Device->CreateCommandAllocator(
        m_commandListType,
        IID_PPV_ARGS( &allocator )
        )
    );
    return allocator;
}

ComPtr<ID3D12GraphicsCommandList> CommandQueue::CreateGraphicsCommandList( ComPtr<ID3D12CommandAllocator> allocator ) const
{
    ComPtr<ID3D12GraphicsCommandList> commandList;
    ThrowIfFailed(
        m_d3d12Device->CreateCommandList(
            0, m_commandListType,
            allocator.Get( ),
            nullptr,
            IID_PPV_ARGS( &commandList )
        )
    );
    return commandList;
}




