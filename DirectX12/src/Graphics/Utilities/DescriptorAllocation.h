#pragma once


#include <Oblivion.h>

class DescriptorAllocatorPage;

class DescriptorAllocation
{
public:
    DescriptorAllocation() = default;
    DescriptorAllocation(CD3DX12_CPU_DESCRIPTOR_HANDLE handle, uint32_t numDescriptors, uint32_t descriptorSize,
        std::shared_ptr<DescriptorAllocatorPage> page);
    ~DescriptorAllocation( );

    DescriptorAllocation( DescriptorAllocation& ) = delete;
    DescriptorAllocation& operator = ( DescriptorAllocation& ) = delete;

    DescriptorAllocation( DescriptorAllocation&& );
    DescriptorAllocation& operator = ( DescriptorAllocation&& );

public:
    bool IsNull() const;
    D3D12_CPU_DESCRIPTOR_HANDLE GetDescriptorHandle( uint32_t offset ) const;
    uint32_t GetNumDescriptors( ) const;
    std::shared_ptr<DescriptorAllocatorPage> GetDescriptorAllocatorPage( ) const;

private:
    void Free( );

    CD3DX12_CPU_DESCRIPTOR_HANDLE m_descriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE( CD3DX12_DEFAULT() );
    uint32_t m_nDescriptors = 0;
    uint32_t m_descriptorSize = 0;
    std::shared_ptr<DescriptorAllocatorPage> m_allocatorPage = nullptr;
};
