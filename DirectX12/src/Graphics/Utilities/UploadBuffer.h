#pragma once


#include "Oblivion.h"



class UploadBuffer
{
public:
    struct Allocation
    {
        void* pCPU;
        D3D12_GPU_VIRTUAL_ADDRESS pGPU;
    };
public:
    explicit UploadBuffer( size_t pageSize );
    ~UploadBuffer( );

public:
    size_t GetPageSize( ) const;
    Allocation Allocate( size_t size, size_t alignment );
    void Reset( );

private:
    struct Page : public std::enable_shared_from_this<Page>
    {
        Page( size_t size );
        ~Page( );
    public:

        bool HasSpace( size_t size, size_t alignment );
        Allocation Allocate( size_t size, size_t alignment );
        void Reset( );

    private:
        Microsoft::WRL::ComPtr<ID3D12Resource> m_commitedResource;
        void* m_pCPUMemoryStart;
        D3D12_GPU_VIRTUAL_ADDRESS m_pGPUMemoryStart;

        size_t m_size;
        size_t m_offset;
    };
    using PagePool = std::deque<std::shared_ptr<Page>>;

    std::shared_ptr<Page> RequestPage( );

private:
    size_t                  m_pageSize;

    PagePool                m_availablePages;
    PagePool                m_pagePool;

    std::shared_ptr<Page>   m_currentPage;
};

