#include "Oblivion.h"
#include "DescriptorAllocator.h"

#include "DescriptorAllocation.h"
#include "DescriptorAllocatorPage.h"

DescriptorAllocator::DescriptorAllocator(D3D12_HEAP_TYPE heapType, uint32_t numDescriptorsPerHeap) :
    m_heapType(heapType), m_numDescriptorsPerHeap(numDescriptorsPerHeap)
{ }

DescriptorAllocation DescriptorAllocator::Allocate(uint32_t numDescriptors)
{
    std::lock_guard<std::mutex> guard(m_allocationMutex);

    DescriptorAllocation allocation;

    for (auto it : m_availableHeaps)
    {
        auto allocatorPage = m_heapPool[it];
        allocation = allocatorPage->Allocate(numDescriptors);
        
        if (allocatorPage->GetFreeHandlesCount() == 0)
        {
            m_availableHeaps.erase(it);
        }
        if (!allocation.IsNull())
        {
            break;
        }
    }
    if (allocation.IsNull())
    { // Failed to allocate from existing pages, create a new page
        m_numDescriptorsPerHeap = std::max(m_numDescriptorsPerHeap, numDescriptors);
        auto page = CreateAllocatorPage();
        page->Allocate(numDescriptors);
    }

    EVALUATE(allocation.IsNull(), true, == , "Unable to allocate a valid descriptor page");
    return allocation;
}

void DescriptorAllocator::ReleaseStaleDescriptors(uint32_t frameNumber)
{
    std::lock_guard<std::mutex> guard(m_allocationMutex);

    for (uint32_t i = 0; i < m_heapPool.size(); ++i)
    {
        auto& page = m_heapPool[i];
        page->ReleaseStaleDescriptors(frameNumber);
        if (page->GetFreeHandlesCount() > 0)
        {
            m_availableHeaps.insert(i);
        }
    }

}

std::shared_ptr<DescriptorAllocatorPage> DescriptorAllocator::CreateAllocatorPage()
{
    auto page = std::make_shared<DescriptorAllocatorPage>();

    m_heapPool.push_back(page);
    m_availableHeaps.insert((uint32_t)m_heapPool.size() - 1);
    
    return page;
}

DescriptorAllocator::~DescriptorAllocator()
{
    m_availableHeaps.clear();
    m_heapPool.clear();
}
