#include "Oblivion.h"
#include "UploadBuffer.h"
#include "../Direct3D.h"

UploadBuffer::UploadBuffer( size_t pageSize ):
    m_pageSize( pageSize )
{ }

UploadBuffer::~UploadBuffer( )
{ }

size_t UploadBuffer::GetPageSize( ) const
{
    return m_pageSize;
}

UploadBuffer::Allocation UploadBuffer::Allocate( size_t size, size_t alignment )
{
    EVALUATE( size, m_pageSize, >= , "Allocation requested size is bigger than a page (%d >= %d)", size, m_pageSize );
    if ( !m_currentPage || !m_currentPage->HasSpace( size, alignment ) )
    {
        m_currentPage = RequestPage( );
    }

    return m_currentPage->Allocate( size, alignment );
}

void UploadBuffer::Reset( )
{
    m_availablePages = m_pagePool;
    for ( auto page : m_availablePages )
    {
        page->Reset( );
    }
}

std::shared_ptr<UploadBuffer::Page> UploadBuffer::RequestPage( )
{
    std::shared_ptr<Page> page;
    if ( !m_availablePages.empty( ) )
    {
        page = m_availablePages.front( );
        m_availablePages.pop_front( );
    }
    else
    {
        page = std::make_shared<Page>( m_pageSize );
        m_pagePool.push_back( page );
    }
    return page;
}

UploadBuffer::Page::Page( size_t size ):
    m_size( size ),
    m_offset( 0 )
{
    auto d3d12Device = Direct3D::Get( )->GetD3D12Device( );
    ThrowIfFailed(
        d3d12Device->CreateCommittedResource(
            &CD3DX12_HEAP_PROPERTIES::CD3DX12_HEAP_PROPERTIES( D3D12_HEAP_TYPE::D3D12_HEAP_TYPE_UPLOAD ),
            D3D12_HEAP_FLAGS::D3D12_HEAP_FLAG_NONE,
            &CD3DX12_RESOURCE_DESC::Buffer( size ),
            D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_GENERIC_READ,
            nullptr,
            IID_PPV_ARGS( &m_commitedResource )
        )
    );

    m_pGPUMemoryStart = m_commitedResource->GetGPUVirtualAddress( );
    ThrowIfFailed(
        m_commitedResource->Map( 0, nullptr, &m_pCPUMemoryStart )
    );
}

UploadBuffer::Page::~Page( )
{
    m_commitedResource->Unmap( 0, nullptr );
    m_pCPUMemoryStart = nullptr;
    m_pGPUMemoryStart = 0;
}

bool UploadBuffer::Page::HasSpace( size_t size, size_t alignment )
{
    size_t alignedSize = Math::AlignUp( size, alignment );
    size_t alignedOffset = Math::AlignUp( m_offset, alignment );

    return alignedSize + alignedOffset <= m_size;
}

UploadBuffer::Allocation UploadBuffer::Page::Allocate( size_t size, size_t alignment )
{
    size_t alignedSize = Math::AlignUp( size, alignment );
    m_offset = Math::AlignUp( m_offset, alignment );

    Allocation allocation;
    allocation.pCPU = static_cast<BYTE*>(m_pCPUMemoryStart) + m_offset;
    allocation.pGPU = m_pGPUMemoryStart + m_offset;

    m_offset += alignedSize;

    return allocation;
}

void UploadBuffer::Page::Reset( )
{
    m_offset = 0;
}


