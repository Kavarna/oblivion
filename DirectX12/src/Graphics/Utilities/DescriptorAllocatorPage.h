#pragma once


#include <Oblivion.h>

#include "DescriptorAllocation.h"


class DescriptorAllocatorPage : public std::enable_shared_from_this<DescriptorAllocatorPage>
{
public:
    DescriptorAllocatorPage( ) = default;
    DescriptorAllocatorPage( D3D12_DESCRIPTOR_HEAP_TYPE heapType, uint32_t numDescriptorsPerHeap );

public:
    DescriptorAllocation Allocate( uint32_t numDescriptors );
    void Free( DescriptorAllocation&& allocation, uint64_t frameNumber );
    void ReleaseStaleDescriptors( uint64_t frameNumber );

    D3D12_DESCRIPTOR_HEAP_TYPE GetHeapType( ) const;
    uint32_t GetFreeHandlesCount( ) const;
    bool HasSpace( uint32_t numDescriptors ) const;

private:
    uint32_t ComputeOffset( D3D12_CPU_DESCRIPTOR_HANDLE handle );
    void AddNewBlock( uint32_t offset, uint32_t numDescriptors );
    void FreeBlock( uint32_t offset, uint32_t numDescriptors );

private:
    using SizeType = uint32_t;
    using OffsetType = uint32_t;

    struct FreeBlockInfo;

    using FreeListByOffset = std::map<OffsetType, FreeBlockInfo>;
    using FreeListBySize = std::multimap<SizeType, FreeListByOffset::iterator>;

    struct FreeBlockInfo
    {
        FreeBlockInfo( SizeType size ) : size( size ) { };
        SizeType size;
        FreeListBySize::iterator freelistBySizeIt;
    };

    struct StaleDescriptorInfo
    {
        StaleDescriptorInfo( SizeType size, OffsetType offset, uint64_t frameNumber ) :
            size( size ), offset( offset ), frameNumber( frameNumber )
        { };
        SizeType size;
        OffsetType offset;
        uint64_t frameNumber;
    };

    using StaleDescriptorsQueue = std::queue<StaleDescriptorInfo>;

    FreeListByOffset m_freelistByOffset;
    FreeListBySize m_freelistBySize;
    StaleDescriptorsQueue m_staleDescriptors;

    Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_d3d12DescriptorHeap;
    D3D12_DESCRIPTOR_HEAP_TYPE m_heapType;
    CD3DX12_CPU_DESCRIPTOR_HANDLE m_baseDescriptorHandle;
    uint32_t m_descriptorIncrementSize;
    uint32_t m_numDescriptorsInHeap;
    uint32_t m_numFreeHandles;

    std::mutex m_allocationMutex;
};

