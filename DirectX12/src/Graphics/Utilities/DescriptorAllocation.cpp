#include "Oblivion.h"
#include "DescriptorAllocation.h"
#include "DescriptorAllocatorPage.h"

#include "../Direct3D.h"
#include "../../Game.h"

DescriptorAllocation::DescriptorAllocation( CD3DX12_CPU_DESCRIPTOR_HANDLE handle, uint32_t numDescriptors, uint32_t descriptorSize, std::shared_ptr<DescriptorAllocatorPage> page ) :
    m_descriptor( handle ), m_nDescriptors( numDescriptors ), m_descriptorSize( descriptorSize ),
    m_allocatorPage( page )
{ }

DescriptorAllocation::~DescriptorAllocation( )
{
    Free( );
}

DescriptorAllocation::DescriptorAllocation( DescriptorAllocation&& allocation ) :
    m_descriptor( allocation.m_descriptor ),
    m_nDescriptors( allocation.m_nDescriptors ),
    m_descriptorSize( allocation.m_descriptorSize ),
    m_allocatorPage( std::move(allocation.m_allocatorPage) )
{
    allocation.m_descriptor.ptr = 0;
    allocation.m_descriptorSize = 0;
    allocation.m_nDescriptors = 0;
}

DescriptorAllocation& DescriptorAllocation::operator=( DescriptorAllocation&& allocation )
{
    Free( );
    m_descriptor = allocation.m_descriptor;
    m_nDescriptors = allocation.m_nDescriptors;
    m_descriptorSize = allocation.m_descriptorSize;
    m_allocatorPage = std::move( allocation.m_allocatorPage );

    allocation.m_descriptor.ptr = 0;
    allocation.m_descriptorSize = 0;
    allocation.m_nDescriptors = 0;

    return *this;
}

bool DescriptorAllocation::IsNull( ) const
{
    return m_descriptor.ptr == 0;
}

D3D12_CPU_DESCRIPTOR_HANDLE DescriptorAllocation::GetDescriptorHandle( uint32_t offset ) const
{
    EVALUATE_DEBUG( offset, m_nDescriptors, >= ,
                    "Offset of descriptor allocation is larger than the actual"\
                    "number of descriptors; %d >= %d", offset, m_nDescriptors );
    return { m_descriptor.ptr + (offset * m_descriptorSize) };
}

uint32_t DescriptorAllocation::GetNumDescriptors( ) const
{
    return m_nDescriptors;
}

std::shared_ptr<DescriptorAllocatorPage> DescriptorAllocation::GetDescriptorAllocatorPage( ) const
{
    return m_allocatorPage;
}

void DescriptorAllocation::Free( )
{
    if ( !IsNull( ) && m_allocatorPage )
    {
        m_allocatorPage->Free( std::move( *this ), Game::Get( )->GetFrameCount( ) );

        m_descriptor.ptr = 0;
        m_descriptorSize = 0;
        m_nDescriptors = 0;
        m_allocatorPage.reset( );
    }
}



