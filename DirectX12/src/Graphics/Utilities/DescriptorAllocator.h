#pragma once

#include <Oblivion.h>
#include "DescriptorAllocation.h"


class DescriptorAllocatorPage;

class DescriptorAllocator
{
public:
    DescriptorAllocator(D3D12_HEAP_TYPE heapType, uint32_t numDescriptorsPerHeap);
    ~DescriptorAllocator();

    DescriptorAllocation Allocate(uint32_t numDescriptors = 1);
    
    void ReleaseStaleDescriptors(uint32_t frameNumber);

private:
    using DescriptorHeapPool = std::vector<std::shared_ptr<DescriptorAllocatorPage>>;

    std::shared_ptr<DescriptorAllocatorPage> CreateAllocatorPage();

    D3D12_HEAP_TYPE m_heapType;
    uint32_t m_numDescriptorsPerHeap;

    DescriptorHeapPool m_heapPool;
    std::set<uint32_t> m_availableHeaps;

    std::mutex m_allocationMutex;
};


