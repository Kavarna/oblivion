#include "Oblivion.h"
#include "DescriptorAllocatorPage.h"

#include "../Direct3D.h"

DescriptorAllocatorPage::DescriptorAllocatorPage( D3D12_DESCRIPTOR_HEAP_TYPE heapType, uint32_t numDescriptorsPerHeap ) :
    m_heapType( heapType ),
    m_numDescriptorsInHeap( numDescriptorsPerHeap ),
    m_numFreeHandles( numDescriptorsPerHeap )
{
    auto device = Direct3D::Get( )->GetD3D12Device( );

    D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
    heapDesc.Type = heapType;
    heapDesc.NumDescriptors = numDescriptorsPerHeap;
    ThrowIfFailed(
        device->CreateDescriptorHeap(
            &heapDesc, IID_PPV_ARGS( &m_d3d12DescriptorHeap )
        )
    );

    m_baseDescriptorHandle = m_d3d12DescriptorHeap->GetCPUDescriptorHandleForHeapStart( );
    m_descriptorIncrementSize = device->GetDescriptorHandleIncrementSize( heapType );

    AddNewBlock( 0, numDescriptorsPerHeap );
}

DescriptorAllocation DescriptorAllocatorPage::Allocate( uint32_t numDescriptors )
{
    std::lock_guard<std::mutex> guard( m_allocationMutex );

    if ( m_numFreeHandles < numDescriptors )
        return DescriptorAllocation( );

    auto smallestBlockIt = m_freelistBySize.lower_bound( numDescriptors );
    if ( smallestBlockIt == m_freelistBySize.end( ) )
        return DescriptorAllocation( );

    auto blockSize = smallestBlockIt->first;
    auto blockOffsetIt = smallestBlockIt->second;
    auto offset = blockOffsetIt->first;

    m_freelistBySize.erase( smallestBlockIt );
    m_freelistByOffset.erase( blockOffsetIt );

    auto newOffset = offset + numDescriptors;
    auto newSize = blockSize - numDescriptors;

    if ( newSize > 0 )
        AddNewBlock( newOffset, newSize );

    m_numFreeHandles -= numDescriptors;

    return DescriptorAllocation(
        CD3DX12_CPU_DESCRIPTOR_HANDLE( m_baseDescriptorHandle, offset, m_descriptorIncrementSize ),
        numDescriptors, m_descriptorIncrementSize, shared_from_this( )
    );
}

void DescriptorAllocatorPage::Free( DescriptorAllocation&& allocation, uint64_t frameNumber )
{
    auto offset = ComputeOffset( allocation.GetDescriptorHandle( 0 ) );

    std::lock_guard<std::mutex> guard( m_allocationMutex );

    m_staleDescriptors.emplace( allocation.GetNumDescriptors( ), offset, frameNumber );
}

void DescriptorAllocatorPage::ReleaseStaleDescriptors( uint64_t frameNumber )
{
    std::lock_guard<std::mutex> guard( m_allocationMutex );

    while ( !m_staleDescriptors.empty( ) && m_staleDescriptors.front( ).frameNumber <= frameNumber )
    {
        auto& descriptor = m_staleDescriptors.front( );
        FreeBlock( descriptor.offset, descriptor.size );
        m_staleDescriptors.pop( );
    }
}

D3D12_DESCRIPTOR_HEAP_TYPE DescriptorAllocatorPage::GetHeapType( ) const
{
    return m_heapType;
}

uint32_t DescriptorAllocatorPage::GetFreeHandlesCount( ) const
{
    return m_numFreeHandles;
}

bool DescriptorAllocatorPage::HasSpace( uint32_t numDescriptors ) const
{
    return m_freelistBySize.lower_bound( numDescriptors ) != m_freelistBySize.end( );
}

uint32_t DescriptorAllocatorPage::ComputeOffset( D3D12_CPU_DESCRIPTOR_HANDLE handle )
{
    return (uint32_t)((handle.ptr - m_baseDescriptorHandle.ptr) / m_descriptorIncrementSize);
}

void DescriptorAllocatorPage::AddNewBlock( uint32_t offset, uint32_t numDescriptors )
{
    auto offsetIt = m_freelistByOffset.emplace( offset, numDescriptors );
    auto sizeIt = m_freelistBySize.emplace( numDescriptors, offsetIt.first );
    offsetIt.first->second.freelistBySizeIt = sizeIt;
}

void DescriptorAllocatorPage::FreeBlock( uint32_t offset, uint32_t numDescriptors )
{
    auto nextBlockIt = m_freelistByOffset.lower_bound( offset );
    auto prevBlockIt = nextBlockIt;

    if ( prevBlockIt != m_freelistByOffset.begin( ) )
    {
        prevBlockIt--;
    }
    else
    {
        prevBlockIt = m_freelistByOffset.end( );
    }
    m_numFreeHandles += numDescriptors;

    if ( prevBlockIt != m_freelistByOffset.end( ) &&
         offset == prevBlockIt->first + prevBlockIt->second.size )
    { // The freed block is exactly after the previous block; Increase previous block size
        offset = prevBlockIt->first;
        numDescriptors += prevBlockIt->second.size;

        m_freelistByOffset.erase( offset );
        m_freelistBySize.erase( prevBlockIt->second.freelistBySizeIt );
    }
    else if ( nextBlockIt != m_freelistByOffset.end( ) &&
              nextBlockIt->first == offset + numDescriptors )
    {
        numDescriptors += nextBlockIt->second.size;

        m_freelistByOffset.erase( nextBlockIt->first );
        m_freelistBySize.erase( nextBlockIt->second.freelistBySizeIt );
    }

    AddNewBlock( offset, numDescriptors );
}
