#pragma once


#include <Oblivion.h>



class CommandQueue
{
public:
    CommandQueue( ComPtr<ID3D12Device6> device, D3D12_COMMAND_LIST_TYPE type );
    virtual ~CommandQueue( );

public:
    ComPtr<ID3D12GraphicsCommandList> GetCommandList( ) const;
    UINT64 ExecuteCommandList( ComPtr<ID3D12GraphicsCommandList> commandList ) const;

    ID3D12CommandQueue* GetD3D12CommandQueue( ) const;

    void WaitForFenceValue( UINT64 fenceValue ) const;
    bool IsFenceCompleted( UINT64 fenceValue ) const;
    void Flush( ) const;

private:
    UINT64 Signal( ) const;

    ComPtr<ID3D12CommandAllocator> CreateCommandAllocator( ) const;
    ComPtr<ID3D12GraphicsCommandList> CreateGraphicsCommandList( ComPtr<ID3D12CommandAllocator> ) const;


private:
    ComPtr<ID3D12Device6> m_d3d12Device;

    D3D12_COMMAND_LIST_TYPE m_commandListType;
    ComPtr<ID3D12CommandQueue> m_d3d12CommandQueue;

    ComPtr<ID3D12Fence> m_fence;
    HANDLE m_fenceEvent;
    
    mutable UINT64 m_fenceValue;

    struct AllocatorFence
    {
        ComPtr<ID3D12CommandAllocator> allocator;
        UINT64 fenceValue;
    };
    mutable std::queue<AllocatorFence> m_queueAllocatorFence;
    mutable std::queue<ComPtr<ID3D12GraphicsCommandList>> m_queueCommandLists;

};


