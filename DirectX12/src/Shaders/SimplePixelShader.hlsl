struct PSIn
{
	float4 Color    : COLOR;
};


float4 main( PSIn input ): SV_Target
{
    return input.Color;
}
