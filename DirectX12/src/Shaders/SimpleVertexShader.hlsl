
struct VSCB
{
	float4x4 WVP;
};

ConstantBuffer<VSCB> ModelViewMatrixCB : register(b0);


struct VSIn
{
	float3 pos : POSITION;
	float3 color : COLOR;
};

struct VSOut
{
	float4 col : COLOR;
	float4 pos : SV_POSITION;
};

VSOut main( VSIn input )
{
	VSOut output;

	output.pos = mul( ModelViewMatrixCB.WVP, float4( input.pos, 1.0f ) );
	output.col = float4(input.color, 1.0f);

	return output;
}