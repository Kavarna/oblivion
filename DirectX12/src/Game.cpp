#include "Oblivion.h"
#include "Game.h"
#include "Graphics/Direct3D.h"

using namespace DirectX;

constexpr wchar_t WINDOW_CLASS_NAME[ ] = L"OblivionWindow";

LRESULT CALLBACK WndProc( HWND, UINT, WPARAM, LPARAM );


struct PositionColorVertex
{
    XMFLOAT3 position;
    XMFLOAT3 color;
};

PositionColorVertex g_Vertices[ 8 ] = {
    { XMFLOAT3( -1.0f, -1.0f, -1.0f ), XMFLOAT3( 0.0f, 0.0f, 0.0f ) }, // 0
    { XMFLOAT3( -1.0f,  1.0f, -1.0f ), XMFLOAT3( 0.0f, 1.0f, 0.0f ) }, // 1
    { XMFLOAT3( 1.0f,  1.0f, -1.0f ), XMFLOAT3( 1.0f, 1.0f, 0.0f ) }, // 2
    { XMFLOAT3( 1.0f, -1.0f, -1.0f ), XMFLOAT3( 1.0f, 0.0f, 0.0f ) }, // 3
    { XMFLOAT3( -1.0f, -1.0f,  1.0f ), XMFLOAT3( 0.0f, 0.0f, 1.0f ) }, // 4
    { XMFLOAT3( -1.0f,  1.0f,  1.0f ), XMFLOAT3( 0.0f, 1.0f, 1.0f ) }, // 5
    { XMFLOAT3( 1.0f,  1.0f,  1.0f ), XMFLOAT3( 1.0f, 1.0f, 1.0f ) }, // 6
    { XMFLOAT3( 1.0f, -1.0f,  1.0f ), XMFLOAT3( 1.0f, 0.0f, 1.0f ) }  // 7
};

DWORD g_Indices[ 36 ] =
{
    0, 1, 2, 0, 2, 3,
    4, 6, 5, 4, 7, 6,
    4, 5, 1, 4, 1, 0,
    3, 2, 6, 3, 6, 7,
    1, 5, 6, 1, 6, 2,
    4, 0, 3, 4, 3, 7
};


Game::Game( HINSTANCE hInstance ):
    m_hinstance( hInstance )
{
    m_isInitialized = false;
    InitWindow( );
    InitD3D( );
    Init3D( );
    m_isInitialized = true;
    OnResize( ResizeEventArgs { m_clientWidth, m_clientHeight } );
}

Game::~Game( )
{
    Direct3D::Get()->FlushAll( );
    Direct3D::reset( );
    UnregisterClass( WINDOW_CLASS_NAME, m_hinstance );
}

void Game::Run( )
{
    UpdateWindow( m_hwnd );
    ShowWindow( m_hwnd, SW_SHOWNORMAL );
    SetFocus( m_hwnd );
    MSG msg = {};
    while ( msg.message != WM_QUIT )
    {
        if ( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
        {
            TranslateMessage( &msg );
            DispatchMessage( &msg );
        }
        else
        {
            Update( );
            Render( );
        }
    }

}

bool Game::IsInitialized( )
{
    return m_isInitialized;
}

void Game::OnResize( const ResizeEventArgs& resizeArgs )
{
    m_clientWidth = resizeArgs.Width; m_clientHeight = resizeArgs.Height;
    if ( m_isInitialized )
    {
        Direct3D::Get( )->FlushAll( );
     
        InitCamera( );

        ResizeBackBuffers( resizeArgs.Width, resizeArgs.Height );
        ResizeDepthStencil( resizeArgs.Width, resizeArgs.Height );
    }
}

void Game::InitWindow( )
{
    WNDCLASSEX wndClass = {};
    wndClass.cbSize = sizeof( wndClass );
    wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wndClass.hInstance = m_hinstance;
    wndClass.lpfnWndProc = WndProc;
    wndClass.lpszClassName = WINDOW_CLASS_NAME;
    wndClass.style = CS_VREDRAW | CS_HREDRAW;
    EVALUATE( RegisterClassEx( &wndClass ), 0, == , "Unable to register class" );

    int screenSizeX = GetSystemMetrics( SM_CXSCREEN );
    int screenSizeY = GetSystemMetrics( SM_CYSCREEN );

    RECT windowRect = { 0,0,(LONG)m_clientWidth, (LONG)m_clientHeight };
    AdjustWindowRect( &windowRect, WS_OVERLAPPEDWINDOW, FALSE );

    int windowWidth = windowRect.right - windowRect.left;
    int windowHeight = windowRect.bottom - windowRect.top;

    int windowX = (int)std::max( { 0.0f, (screenSizeX - windowWidth) / 2.0f } );
    int windowY = (int)std::max( { 0.0f, (screenSizeY - windowHeight) / 2.0f } );

    m_hwnd = CreateWindow(
        WINDOW_CLASS_NAME, L"Oblivion",
        WS_OVERLAPPEDWINDOW,
        windowX, windowY,
        windowWidth, windowHeight,
        NULL, NULL, m_hinstance, NULL
    );

    EVALUATE( m_hwnd, NULL, == , "Unable to create window" );
}

void Game::InitD3D( )
{
    EVALUATE( DirectX::XMVerifyCPUSupport( ), false, == , "CPU doesn't offer support for DirectX math" );

    Direct3D::Get( false );
    m_backBufferHeap = Direct3D::Get( )->CreateDescriptorHeap( kNumBackBuffers,
                                                               D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_RTV );
    
    m_backBufferSize = Direct3D::Get( )->GetDescriptorIncreaseSize( D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_RTV );

    m_swapChain = CreateSwapChain( );
}

void Game::Init3D( )
{
    auto renderer = Direct3D::Get( );
    auto d3d12Device = renderer->GetD3D12Device( );
    auto commandQueue = renderer->GetCommandQueue<D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_COPY>( );
    auto commandList = commandQueue->GetCommandList( );

    // Create vertex buffer
    ComPtr<ID3D12Resource> intermediateVertexBuffer;
    UpdateBufferResource( commandList, &m_vertexBuffer, &intermediateVertexBuffer,
                          _countof( g_Vertices ), sizeof( g_Vertices[ 0 ] ), g_Vertices );
    m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress( );
    m_vertexBufferView.SizeInBytes = sizeof( g_Vertices );
    m_vertexBufferView.StrideInBytes = sizeof( g_Vertices[ 0 ] );

    // Create index buffer
    ComPtr<ID3D12Resource> intermediateIndexBuffer;
    UpdateBufferResource( commandList, &m_indexBuffer, &intermediateIndexBuffer,
                          _countof( g_Indices ), sizeof( g_Indices[ 0 ] ), g_Indices );
    m_indexBufferView.BufferLocation = m_indexBuffer->GetGPUVirtualAddress( );
    m_indexBufferView.Format = DXGI_FORMAT::DXGI_FORMAT_R32_UINT;
    m_indexBufferView.SizeInBytes = sizeof( g_Indices );


    // Create depth stencil view
    m_DSVHeap = renderer->CreateDescriptorHeap(
        1, D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_DSV
    );

    // Load shaders
    ComPtr<ID3DBlob> vertexShaderBlob;
    ThrowIfFailed(
        D3DReadFileToBlob( L"Shaders\\SimpleVertexShader.cso", &vertexShaderBlob )
    );

    ComPtr<ID3DBlob> pixelShaderBlob;
    ThrowIfFailed(
        D3DReadFileToBlob( L"Shaders\\SimplePixelShader.cso", &pixelShaderBlob )
    );

    // Define input layout
    D3D12_INPUT_ELEMENT_DESC inputElements[ 2 ];
    inputElements[ 0 ].AlignedByteOffset = D3D12_APPEND_ALIGNED_ELEMENT;
    inputElements[ 0 ].Format = DXGI_FORMAT::DXGI_FORMAT_R32G32B32_FLOAT;
    inputElements[ 0 ].InputSlot = 0;
    inputElements[ 0 ].InputSlotClass = D3D12_INPUT_CLASSIFICATION::D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA;
    inputElements[ 0 ].InstanceDataStepRate = 0;
    inputElements[ 0 ].SemanticIndex = 0;
    inputElements[ 0 ].SemanticName = "POSITION";
    inputElements[ 1 ].AlignedByteOffset = D3D12_APPEND_ALIGNED_ELEMENT;
    inputElements[ 1 ].Format = DXGI_FORMAT::DXGI_FORMAT_R32G32B32_FLOAT;
    inputElements[ 1 ].InputSlot = 0;
    inputElements[ 1 ].InputSlotClass = D3D12_INPUT_CLASSIFICATION::D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA;
    inputElements[ 1 ].InstanceDataStepRate = 0;
    inputElements[ 1 ].SemanticIndex = 0;
    inputElements[ 1 ].SemanticName = "COLOR";
    D3D12_INPUT_LAYOUT_DESC layoutDesc;
    layoutDesc.NumElements = _countof( inputElements );
    layoutDesc.pInputElementDescs = inputElements;

    // Query for ROOT SIG 1.1 support
    D3D12_FEATURE_DATA_ROOT_SIGNATURE featureData = {};
    featureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_1;
    if ( FAILED( d3d12Device->CheckFeatureSupport( D3D12_FEATURE::D3D12_FEATURE_ROOT_SIGNATURE, &featureData, sizeof( featureData ) ) ) )
    {
        featureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_0;
    }

    // Create root signature
    D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
        D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS;

    CD3DX12_ROOT_PARAMETER1 rootParameters[ 1 ];
    rootParameters[ 0 ].InitAsConstants( sizeof( DirectX::XMMATRIX ) / 4, 0, 0, D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_VERTEX );

    CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignature;
    rootSignature.Init_1_1( _countof( rootParameters ), rootParameters,
                            0, nullptr, rootSignatureFlags );

    ComPtr<ID3DBlob> errorBlob;
    ComPtr<ID3DBlob> rootSignatureBlob;
    ThrowIfFailed(
        D3DX12SerializeVersionedRootSignature( &rootSignature,
                                               featureData.HighestVersion,
                                               &rootSignatureBlob, &errorBlob )
    );
    ThrowIfFailed(
        d3d12Device->CreateRootSignature(
            0,
            rootSignatureBlob->GetBufferPointer( ),
            rootSignatureBlob->GetBufferSize( ),
            IID_PPV_ARGS( &m_rootSignature )
        )
    );

    // Create Pipeline State Object (PSO)
    struct PipelineStateObject
    {
        CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE rootSignature;
        CD3DX12_PIPELINE_STATE_STREAM_INPUT_LAYOUT inputLayout;
        CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY primitiveTopology;
        CD3DX12_PIPELINE_STATE_STREAM_VS VS;
        CD3DX12_PIPELINE_STATE_STREAM_PS PS;
        CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL_FORMAT DSVformat;
        CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVformats;
    } pipelineStateStream;
    D3D12_RT_FORMAT_ARRAY renderTargets = { };
    renderTargets.NumRenderTargets = 1;
    renderTargets.RTFormats[ 0 ] = DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM;

    pipelineStateStream.DSVformat = DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT;
    pipelineStateStream.inputLayout = layoutDesc;
    pipelineStateStream.primitiveTopology = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
    pipelineStateStream.VS = CD3DX12_SHADER_BYTECODE{ vertexShaderBlob.Get() };
    pipelineStateStream.PS = CD3DX12_SHADER_BYTECODE { pixelShaderBlob.Get( ) };
    pipelineStateStream.rootSignature = m_rootSignature.Get( );
    pipelineStateStream.RTVformats = renderTargets;

    D3D12_PIPELINE_STATE_STREAM_DESC stateStreamDesc;
    stateStreamDesc.SizeInBytes = sizeof( PipelineStateObject );
    stateStreamDesc.pPipelineStateSubobjectStream = &pipelineStateStream;

    ThrowIfFailed(
        d3d12Device->CreatePipelineState( &stateStreamDesc, IID_PPV_ARGS( &m_pipelineState ) )
    );

    UINT64 fenceValue = commandQueue->ExecuteCommandList( commandList );
    commandQueue->WaitForFenceValue( fenceValue );
}

void Game::InitCamera( )
{
    m_fov = DirectX::XM_PI / 4.f;
    m_world = DirectX::XMMatrixIdentity( );
    m_view = DirectX::XMMatrixLookAtLH( DirectX::XMVectorSet( 0.0f, 5.0f, -3.0f, 1.0f ),
                                        DirectX::XMVectorSet( 0.0f, 0.0f, 0.0f, 1.0f ),
                                        DirectX::XMVectorSet( 0.0f, 1.0f, 0.0f, 1.0f ) );
    m_projection = DirectX::XMMatrixPerspectiveFovLH( m_fov, (float)m_clientWidth / (float)m_clientHeight, 0.1f, 100.f );


    m_viewport.TopLeftX = 0; m_viewport.TopLeftY = 0;
    m_viewport.Height = (float)m_clientHeight; m_viewport.Width = (float)m_clientWidth;
    m_viewport.MinDepth = 0.0f; m_viewport.MaxDepth = 1.0f;

    m_scissorRect = { 0, 0, m_clientWidth, m_clientHeight };
}

void Game::Update( )
{
    static uint64_t frameCount = 0;
    static double totalTime = 0.0;

    m_timer.Tick( );

    frameCount++; // To get FPS
    m_frameCount++; // To get frame count
    totalTime += m_timer.GetDeltaSeconds( );

    if ( totalTime >= 1.0f )
    {
        double fps = frameCount / totalTime;

        Logger::DebugPrint( "FPS: ", fps, "\n" );
        SetWindowTextA( m_hwnd, appendToString( "Oblivion: FPS = ", fps ).c_str( ) );

        frameCount = 0;
        totalTime = 0.0f;
    }
    
    m_world = XMMatrixRotationX( (float)m_timer.GetTotalSeconds( ) );
    m_world *= XMMatrixRotationZ( (float)m_timer.GetTotalSeconds( ) );
}

void Game::Render( )
{
    auto renderer = Direct3D::Get( );
    auto commandQueue = renderer->GetCommandQueue<D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_DIRECT>( );
    auto commandList = commandQueue->GetCommandList( );
    auto backBuffer = m_backBufferResources[ m_activeBackBufferIndex ];
    CD3DX12_CPU_DESCRIPTOR_HANDLE backbufferDescriptorHandle(
        m_backBufferHeap->GetCPUDescriptorHandleForHeapStart( ),
        m_activeBackBufferIndex,
        m_backBufferSize
    );
    auto dsvDescriptorHandle = m_DSVHeap->GetCPUDescriptorHandleForHeapStart( );

    { // Clear the backbuffer
        TransitionResource( commandList, backBuffer,
                            D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_PRESENT,
                            D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_RENDER_TARGET );

        static float red = 0.0f, green = 0.0f, blue = 0.0f;
        static int redDelta = 1, greenDelta = 1, blueDelta = 1;

        red += redDelta * (float)m_timer.GetDeltaSeconds( ) * 0.2f;
        green += greenDelta * (float)m_timer.GetDeltaSeconds( ) * 0.3f;
        blue += blueDelta * (float)m_timer.GetDeltaSeconds( ) * 0.4f;

        if ( red > 1.0f || red < 0.0f )
            redDelta *= -1;
        if ( green > 1.0f || green < 0.0f )
            greenDelta *= -1;
        if ( blue > 1.0f || blue < 0.0f )
            blueDelta *= -1;


        FLOAT clearColor[ 4 ] = { red,green,blue,1.0f };
        commandList->ClearRenderTargetView( backbufferDescriptorHandle, clearColor, 0, nullptr );
        commandList->ClearDepthStencilView( dsvDescriptorHandle,
                                            D3D12_CLEAR_FLAGS::D3D12_CLEAR_FLAG_DEPTH,
                                            1.0f, 0, 0, nullptr );
    }

    commandList->SetGraphicsRootSignature( m_rootSignature.Get( ) );
    commandList->SetPipelineState( m_pipelineState.Get( ) );

    commandList->IASetPrimitiveTopology( D3D12_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
    commandList->IASetVertexBuffers( 0, 1, &m_vertexBufferView );
    commandList->IASetIndexBuffer( &m_indexBufferView );

    commandList->RSSetViewports( 1, &m_viewport );
    commandList->RSSetScissorRects( 1, &m_scissorRect );

    commandList->OMSetRenderTargets( 1, &backbufferDescriptorHandle, FALSE,
                                     &dsvDescriptorHandle );

    XMMATRIX wvp = XMMatrixMultiply( m_world, m_view );
    wvp = XMMatrixMultiply( wvp, m_projection );
    commandList->SetGraphicsRoot32BitConstants( 0, sizeof( XMMATRIX ) / 4, &wvp, 0 );
    
    commandList->DrawIndexedInstanced( _countof( g_Indices ), 1, 0, 0, 0 );

    { // Present the backbuffer
        TransitionResource( commandList, backBuffer,
                            D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_RENDER_TARGET,
                            D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_PRESENT );

        UINT64 fenceValue = commandQueue->ExecuteCommandList( commandList );

        UINT syncInterval = m_useVsync ? 1 : 0;
        UINT presentFlags = !m_useVsync && renderer->HasTearingSupport( ) ? DXGI_PRESENT_ALLOW_TEARING : 0;

        ThrowIfFailed( m_swapChain->Present( syncInterval, presentFlags ) );

        m_activeBackBufferIndex = m_swapChain->GetCurrentBackBufferIndex( );

        commandQueue->WaitForFenceValue( fenceValue );
    }

}

void Game::ResizeBackBuffers( int width, int height )
{
    auto renderer = Direct3D::Get( );
    auto d3d12Device = Direct3D::Get( )->GetD3D12Device( );

    for ( UINT i = 0; i < kNumBackBuffers; ++i )
    {
        m_backBufferResources[ i ].Reset( );
        m_backBufferFenceValues[ i ] = m_backBufferFenceValues[ m_activeBackBufferIndex ];
    }

    DXGI_SWAP_CHAIN_DESC1 swapChainDesc1 = {};
    ThrowIfFailed( m_swapChain->GetDesc1( &swapChainDesc1 ) );
    ThrowIfFailed( m_swapChain->ResizeBuffers(
        kNumBackBuffers, width, height, swapChainDesc1.Format, swapChainDesc1.Flags
    ) );


    CD3DX12_CPU_DESCRIPTOR_HANDLE cpuDescriptorHandle( m_backBufferHeap->GetCPUDescriptorHandleForHeapStart( ) );
    for ( uint32_t i = 0; i < kNumBackBuffers; ++i )
    {
        ComPtr<ID3D12Resource> resource;
        ThrowIfFailed(
            m_swapChain->GetBuffer(
                i,
                IID_PPV_ARGS( &resource )
            )
        );
        m_backBufferResources[ i ] = resource;

        d3d12Device->CreateRenderTargetView( resource.Get( ), nullptr, cpuDescriptorHandle );
        cpuDescriptorHandle.Offset( renderer->GetDescriptorIncreaseSize( D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_RTV ) );
    }

    m_activeBackBufferIndex = m_swapChain->GetCurrentBackBufferIndex( );
}

void Game::ResizeDepthStencil( int width, int height )
{
    auto renderer = Direct3D::Get( );
    auto d3d12Device = renderer->GetD3D12Device( );
    D3D12_CLEAR_VALUE optimizedClearValue;
    optimizedClearValue.DepthStencil.Depth = 1.0f;
    optimizedClearValue.DepthStencil.Stencil = 0;
    optimizedClearValue.Format = DXGI_FORMAT_D32_FLOAT;

    ThrowIfFailed(
        d3d12Device->CreateCommittedResource(
            &CD3DX12_HEAP_PROPERTIES( D3D12_HEAP_TYPE::D3D12_HEAP_TYPE_DEFAULT ),
            D3D12_HEAP_FLAGS::D3D12_HEAP_FLAG_NONE,
            &CD3DX12_RESOURCE_DESC::Tex2D( DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT,
                                           width, height,
                                           1, 0, 1, 0,
                                           D3D12_RESOURCE_FLAGS::D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL ),
            D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_DEPTH_WRITE,
            &optimizedClearValue,
            IID_PPV_ARGS( &m_depthBuffer )
        )
    );

    D3D12_DEPTH_STENCIL_VIEW_DESC dsv;
    dsv.ViewDimension = D3D12_DSV_DIMENSION::D3D12_DSV_DIMENSION_TEXTURE2D;
    dsv.Texture2D.MipSlice = 0;
    dsv.Format = DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT;
    dsv.Flags = D3D12_DSV_FLAGS::D3D12_DSV_FLAG_READ_ONLY_DEPTH;

    d3d12Device->CreateDepthStencilView( m_depthBuffer.Get( ), &dsv,
                                         m_DSVHeap->GetCPUDescriptorHandleForHeapStart( ) );
}

void Game::TransitionResource( ComPtr<ID3D12GraphicsCommandList> commandList, ComPtr<ID3D12Resource> resource,
                               D3D12_RESOURCE_STATES beforeState, D3D12_RESOURCE_STATES afterState )
{
    CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(
        resource.Get( ),
        beforeState, afterState
    );

    commandList->ResourceBarrier( 1, &barrier );

}

void Game::UpdateBufferResource( ComPtr<ID3D12GraphicsCommandList> commandList,
                                 ID3D12Resource** destinationResource,
                                 ID3D12Resource** intermediateResource,
                                 size_t numElements, size_t elementSize,
                                 const void* bufferData, D3D12_RESOURCE_FLAGS flags )
{
    auto device = Direct3D::Get( )->GetD3D12Device( );

    size_t bufferSize = numElements * elementSize;

    ThrowIfFailed(
        device->CreateCommittedResource(
            &CD3DX12_HEAP_PROPERTIES( D3D12_HEAP_TYPE::D3D12_HEAP_TYPE_DEFAULT ),
            D3D12_HEAP_FLAGS::D3D12_HEAP_FLAG_NONE,
            &CD3DX12_RESOURCE_DESC::Buffer( bufferSize, flags ),
            D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_COPY_DEST,
            nullptr,
            IID_PPV_ARGS( destinationResource )
        )
    );

    if ( bufferData )
    {
        ThrowIfFailed(
            device->CreateCommittedResource(
                &CD3DX12_HEAP_PROPERTIES( D3D12_HEAP_TYPE::D3D12_HEAP_TYPE_UPLOAD ),
                D3D12_HEAP_FLAGS::D3D12_HEAP_FLAG_NONE,
                &CD3DX12_RESOURCE_DESC::Buffer( bufferSize ),
                D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_GENERIC_READ,
                nullptr,
                IID_PPV_ARGS( intermediateResource )
            )
        );

        D3D12_SUBRESOURCE_DATA subresourceData;
        subresourceData.pData = bufferData;
        subresourceData.RowPitch = bufferSize;
        subresourceData.SlicePitch = subresourceData.RowPitch;

        UpdateSubresources(
            commandList.Get( ),
            *destinationResource, *intermediateResource,
            0, 0, 1, &subresourceData
        );

    }
}

uint64_t Game::GetFrameCount( )
{
    return m_frameCount;
}

ComPtr<IDXGISwapChain4> Game::CreateSwapChain( )
{
    auto renderer = Direct3D::Get( );
    auto directCommandQueue = renderer->GetCommandQueue<D3D12_COMMAND_LIST_TYPE_DIRECT>( );
    ComPtr<IDXGIFactory7> factory7;
    ThrowIfFailed(
        CreateDXGIFactory1( IID_PPV_ARGS( &factory7 ) )
    );

    DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
    swapChainDesc.AlphaMode = DXGI_ALPHA_MODE::DXGI_ALPHA_MODE_IGNORE;
    swapChainDesc.BufferCount = kNumBackBuffers;
    swapChainDesc.BufferUsage = DXGI_USAGE_BACK_BUFFER;
    swapChainDesc.Flags = renderer->HasTearingSupport( ) ? DXGI_SWAP_CHAIN_FLAG::DXGI_SWAP_CHAIN_FLAG_ALLOW_TEARING : 0;
    swapChainDesc.Format = DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM;
    swapChainDesc.SampleDesc = { 1,0 };
    swapChainDesc.Scaling = DXGI_SCALING::DXGI_SCALING_STRETCH;
    swapChainDesc.Stereo = FALSE;
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT::DXGI_SWAP_EFFECT_FLIP_DISCARD;

    ComPtr<IDXGISwapChain1> swapchain1;
    ThrowIfFailed(
        factory7->CreateSwapChainForHwnd(
            directCommandQueue->GetD3D12CommandQueue( ),
            m_hwnd,
            &swapChainDesc,
            nullptr,
            nullptr,
            &swapchain1
        )
    );

    ComPtr<IDXGISwapChain4> swapchain4;
    ThrowIfFailed(
        swapchain1.As( &swapchain4 )
    );

    return swapchain4;
}


LRESULT CALLBACK WndProc( HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam )
{
    Game* game = Game::Get( );
    switch ( message )
    {
        case WM_SYSKEYDOWN:
        case WM_KEYDOWN:
        {
            MSG charMsg;
            // Get the Unicode character (UTF-16)
            unsigned int c = 0;
            // For printable characters, the next message will be WM_CHAR.
            // This message contains the character code we need to send the KeyPressed event.
            // Inspired by the SDL 1.2 implementation.
            if ( PeekMessage( &charMsg, hwnd, 0, 0, PM_NOREMOVE ) && charMsg.message == WM_CHAR )
            {
                GetMessage( &charMsg, hwnd, 0, 0 );
                c = static_cast<unsigned int>(charMsg.wParam);
            }
            bool shift = (GetAsyncKeyState( VK_SHIFT ) & 0x8000) != 0;
            bool control = (GetAsyncKeyState( VK_CONTROL ) & 0x8000) != 0;
            bool alt = (GetAsyncKeyState( VK_MENU ) & 0x8000) != 0;
            KeyCode::Key key = (KeyCode::Key)wParam;
            unsigned int scanCode = (lParam & 0x00FF0000) >> 16;
            KeyEventArgs keyEventArgs( key, c, KeyEventArgs::Pressed, shift, control, alt );
            // pWindow->OnKeyPressed( keyEventArgs );
        }
        break;
        case WM_SYSKEYUP:
        case WM_KEYUP:
        {
            bool shift = (GetAsyncKeyState( VK_SHIFT ) & 0x8000) != 0;
            bool control = (GetAsyncKeyState( VK_CONTROL ) & 0x8000) != 0;
            bool alt = (GetAsyncKeyState( VK_MENU ) & 0x8000) != 0;
            KeyCode::Key key = (KeyCode::Key)wParam;
            unsigned int c = 0;
            unsigned int scanCode = (lParam & 0x00FF0000) >> 16;

            // Determine which key was released by converting the key code and the scan code
            // to a printable character (if possible).
            // Inspired by the SDL 1.2 implementation.
            unsigned char keyboardState[ 256 ];
            GetKeyboardState( keyboardState );
            wchar_t translatedCharacters[ 4 ];
            if ( int result = ToUnicodeEx( static_cast<UINT>(wParam), scanCode, keyboardState, translatedCharacters, 4, 0, NULL ) > 0 )
            {
                c = translatedCharacters[ 0 ];
            }

            KeyEventArgs keyEventArgs( key, c, KeyEventArgs::Released, shift, control, alt );
            // pWindow->OnKeyReleased( keyEventArgs );
        }
        break;
        // The default window procedure will play a system notification sound 
        // when pressing the Alt+Enter keyboard combination if this message is 
        // not handled.
        case WM_SYSCHAR:
            break;
        case WM_MOUSEMOVE:
        {
            bool lButton = (wParam & MK_LBUTTON) != 0;
            bool rButton = (wParam & MK_RBUTTON) != 0;
            bool mButton = (wParam & MK_MBUTTON) != 0;
            bool shift = (wParam & MK_SHIFT) != 0;
            bool control = (wParam & MK_CONTROL) != 0;

            int x = ((int)(short)LOWORD( lParam ));
            int y = ((int)(short)HIWORD( lParam ));

            MouseMotionEventArgs mouseMotionEventArgs( lButton, mButton, rButton, control, shift, x, y );
            // pWindow->OnMouseMoved( mouseMotionEventArgs );
        }
        break;
        case WM_LBUTTONDOWN:
        case WM_RBUTTONDOWN:
        case WM_MBUTTONDOWN:
        {
            bool lButton = (wParam & MK_LBUTTON) != 0;
            bool rButton = (wParam & MK_RBUTTON) != 0;
            bool mButton = (wParam & MK_MBUTTON) != 0;
            bool shift = (wParam & MK_SHIFT) != 0;
            bool control = (wParam & MK_CONTROL) != 0;

            int x = ((int)(short)LOWORD( lParam ));
            int y = ((int)(short)HIWORD( lParam ));

            /*MouseButtonEventArgs mouseButtonEventArgs( DecodeMouseButton( message ), MouseButtonEventArgs::Pressed, lButton, mButton, rButton, control, shift, x, y );
            pWindow->OnMouseButtonPressed( mouseButtonEventArgs );*/
        }
        break;
        case WM_LBUTTONUP:
        case WM_RBUTTONUP:
        case WM_MBUTTONUP:
        {
            bool lButton = (wParam & MK_LBUTTON) != 0;
            bool rButton = (wParam & MK_RBUTTON) != 0;
            bool mButton = (wParam & MK_MBUTTON) != 0;
            bool shift = (wParam & MK_SHIFT) != 0;
            bool control = (wParam & MK_CONTROL) != 0;

            int x = ((int)(short)LOWORD( lParam ));
            int y = ((int)(short)HIWORD( lParam ));

            /*MouseButtonEventArgs mouseButtonEventArgs( DecodeMouseButton( message ), MouseButtonEventArgs::Released, lButton, mButton, rButton, control, shift, x, y );
            pWindow->OnMouseButtonReleased( mouseButtonEventArgs );*/
        }
        break;
        case WM_MOUSEWHEEL:
        {
            // The distance the mouse wheel is rotated.
            // A positive value indicates the wheel was rotated to the right.
            // A negative value indicates the wheel was rotated to the left.
            float zDelta = ((int)(short)HIWORD( wParam )) / (float)WHEEL_DELTA;
            short keyStates = (short)LOWORD( wParam );

            bool lButton = (keyStates & MK_LBUTTON) != 0;
            bool rButton = (keyStates & MK_RBUTTON) != 0;
            bool mButton = (keyStates & MK_MBUTTON) != 0;
            bool shift = (keyStates & MK_SHIFT) != 0;
            bool control = (keyStates & MK_CONTROL) != 0;

            int x = ((int)(short)LOWORD( lParam ));
            int y = ((int)(short)HIWORD( lParam ));

            // Convert the screen coordinates to client coordinates.
            POINT clientToScreenPoint;
            clientToScreenPoint.x = x;
            clientToScreenPoint.y = y;
            ScreenToClient( hwnd, &clientToScreenPoint );

            MouseWheelEventArgs mouseWheelEventArgs( zDelta, lButton, mButton, rButton, control, shift, (int)clientToScreenPoint.x, (int)clientToScreenPoint.y );
            // pWindow->OnMouseWheel( mouseWheelEventArgs );
        }
        break;
        case WM_SIZE:
        {
            int width = ((int)(short)LOWORD( lParam ));
            int height = ((int)(short)HIWORD( lParam ));

            ResizeEventArgs resizeEventArgs( width, height );
            game->OnResize( resizeEventArgs );
        }
        break;
        case WM_DESTROY:
        {
            PostQuitMessage( 0 );
        }
        break;
        default:
            return DefWindowProcW( hwnd, message, wParam, lParam );
    }
    return 0;
}