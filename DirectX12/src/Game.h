#pragma once


#include <Oblivion.h>
#include "Utilities/HighResolutionTimer.h"


class Game: public ISingletone<Game>
{
    static constexpr const uint32_t kNumBackBuffers = 3;
public:
    Game( HINSTANCE hInstance );
    ~Game( );

public:
    void Run( );

public:
    bool IsInitialized( );
    uint64_t GetFrameCount( );
    void OnResize( const ResizeEventArgs& resizeArgs );

private:
    void InitWindow( );
    void InitD3D( );
    void Init3D( );
    void InitCamera( );

    void Update( );
    void Render( );

    void ResizeBackBuffers( int width, int height );
    void ResizeDepthStencil( int width, int height );

    void TransitionResource( ComPtr<ID3D12GraphicsCommandList> commandList,
                             ComPtr<ID3D12Resource> resource,
                             D3D12_RESOURCE_STATES beforeState,
                             D3D12_RESOURCE_STATES afterState );

    void UpdateBufferResource( ComPtr<ID3D12GraphicsCommandList> commandList,
                               ID3D12Resource** destinationResource,
                               ID3D12Resource** intermediateResource,
                               size_t numElements, size_t elementSize, const void* bufferData,
                               D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAGS::D3D12_RESOURCE_FLAG_NONE );

private:
    ComPtr<IDXGISwapChain4> CreateSwapChain( );

private:
    // Windows
    HWND m_hwnd;
    HINSTANCE m_hinstance;

    // Swapchain
    ComPtr<IDXGISwapChain4> m_swapChain;
    bool m_useVsync = false;

    // Backbuffers
    ComPtr<ID3D12DescriptorHeap> m_backBufferHeap;
    ComPtr<ID3D12Resource> m_backBufferResources[ kNumBackBuffers ];
    UINT m_backBufferSize;
    UINT64 m_backBufferFenceValues[ kNumBackBuffers ];
    UINT m_activeBackBufferIndex;

    // Model
    ComPtr<ID3D12Resource> m_vertexBuffer;
    D3D12_VERTEX_BUFFER_VIEW m_vertexBufferView;
    ComPtr<ID3D12Resource> m_indexBuffer;
    D3D12_INDEX_BUFFER_VIEW m_indexBufferView;

    // Depth
    ComPtr<ID3D12Resource> m_depthBuffer;
    ComPtr<ID3D12DescriptorHeap> m_DSVHeap;

    // Pipeline
    ComPtr<ID3D12RootSignature> m_rootSignature;
    ComPtr<ID3D12PipelineState> m_pipelineState;
    D3D12_VIEWPORT m_viewport;
    D3D12_RECT m_scissorRect;

    // Camera
    float m_fov;
    DirectX::XMMATRIX m_world;
    DirectX::XMMATRIX m_view;
    DirectX::XMMATRIX m_projection;

    // Window
    int m_clientWidth = 1280;
    int m_clientHeight = 720;

    // Misc
    bool m_isInitialized = false;

    HighResolutionTimer m_timer;

    uint64_t m_frameCount = 0;

};



