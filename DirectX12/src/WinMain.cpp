#include "Oblivion.h"
#include "Game.h"
#include <dxgidebug.h>


int WINAPI wWinMain( HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPWSTR commandLine,
                     int iShow )
{
    try
    {
        Game::Get( hInstance )->Run( );
        Game::reset( );
    }
    catch ( const std::exception & e )
    {
        OutputDebugStringA( e.what( ) );
        OutputDebugStringA( "\n" );
    }
    IDXGIDebug1* debugInterface;
    DXGIGetDebugInterface1( 0, IID_PPV_ARGS( &debugInterface ) );

    debugInterface->ReportLiveObjects( DXGI_DEBUG_ALL, DXGI_DEBUG_RLO_FLAGS::DXGI_DEBUG_RLO_ALL );
    debugInterface->Release( );
    return 0;
}


