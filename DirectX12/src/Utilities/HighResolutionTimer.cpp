#include "Oblivion.h"
#include "HighResolutionTimer.h"

HighResolutionTimer::HighResolutionTimer( )
    : m_DeltaTime( 0 )
    , m_TotalTime( 0 )
{
    m_T0 = std::chrono::high_resolution_clock::now( );
}

void HighResolutionTimer::Tick( )
{
    auto t1 = std::chrono::high_resolution_clock::now( );
    m_DeltaTime = t1 - m_T0;
    m_TotalTime += m_DeltaTime;
    m_T0 = t1;
}

void HighResolutionTimer::Reset( )
{
    m_T0 = std::chrono::high_resolution_clock::now( );
    m_DeltaTime = std::chrono::high_resolution_clock::duration( );
    m_TotalTime = std::chrono::high_resolution_clock::duration( );
}

double HighResolutionTimer::GetDeltaNanoseconds( ) const
{
    return m_DeltaTime.count( ) * 1.0;
}
double HighResolutionTimer::GetDeltaMicroseconds( ) const
{
    return m_DeltaTime.count( ) * 1e-3;
}

double HighResolutionTimer::GetDeltaMilliseconds( ) const
{
    return m_DeltaTime.count( ) * 1e-6;
}

double HighResolutionTimer::GetDeltaSeconds( ) const
{
    return m_DeltaTime.count( ) * 1e-9;
}

double HighResolutionTimer::GetTotalNanoseconds( ) const
{
    return m_TotalTime.count( ) * 1.0;
}

double HighResolutionTimer::GetTotalMicroseconds( ) const
{
    return m_TotalTime.count( ) * 1e-3;
}

double HighResolutionTimer::GetTotalMilliSeconds( ) const
{
    return m_TotalTime.count( ) * 1e-6;
}

double HighResolutionTimer::GetTotalSeconds( ) const
{
    return m_TotalTime.count( ) * 1e-9;
}
