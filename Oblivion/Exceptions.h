#pragma once

#include <exception>
#include <comdef.h>
#include <corecrt_wstring.h>
#include "Conversions.h"

namespace Exceptions
{
	class Exception: public std::exception
	{
	protected:
		std::wstring m_data;
	private:
		mutable std::string m_sdata;
	public:
		virtual const char* what( ) const
		{
			m_sdata = Conversions::ws2s( m_data );

			return m_sdata.c_str( );
		}
	};

	class InitializationException: public Exception
	{
	public:
		InitializationException( const wchar_t* message, int line, const wchar_t* file )
		{
			m_data = std::wstring( L"Error: \"" ) + std::wstring( message ) + std::wstring( L"\" on line " ) + std::to_wstring( line ) +
				L" in file " + std::wstring( file );
		}
	};

	class MemoryAllocationException: public Exception
	{
	public:
		MemoryAllocationException( int allocateSize, int line, const wchar_t* file )
		{
			m_data = std::wstring( L"Error: \"Couldn't allocate " ) + std::to_wstring( allocateSize ) +
				std::wstring( L"\" on line " ) + std::to_wstring( line ) +
				L" in file" + std::wstring( file );
		}
	};

	class FailedHResultException: public Exception
	{
	public:
		FailedHResultException( HRESULT hr )
		{
			_com_error err( hr );
			std::wstring errMessage( err.ErrorMessage( ) );
			m_data = std::wstring( L"Error: code " ) + std::to_wstring( hr ) + L" message: " + errMessage;
		}
	};
}

template <class ... Args>
constexpr void getInitializationError( Args... arg )
{
	return appendToString( arg );
}


#define THROW_ERROR(...) {char message[1024];\
sprintf_s(message,sizeof(message),__VA_ARGS__);\
std::exception error(message);\
throw error;}
#define THROW_INITIALIZATION_EXCEPTION(...) {\
char message[1024];\
sprintf_s(message, sizeof(message), __VA_ARGS__);\
throw Exceptions::InitializationException(message, __LINE__, __FILE__)\
;}
#define THROW_ALLOCATION_EXCEPTION(size) {\
throw Exceptions::MemoryAllocationException(size, __LINE__, __FILE__);\
}

#define EVALUATE(expression, expected_result, op, ...) { auto value = (expression); if ( value op decltype(value)(expected_result) ) THROW_ERROR(__VA_ARGS__); }
#if DEBUG || _DEBUG
#define EVALUATE_DEBUG(expression, expected_result, op, ...) if ( (expression) op (expected_result)) THROW_ERROR(__VA_ARGS__);
#else
#define EVALUATE_DEBUG(expression, expected_result, op, ...)
#endif


inline void ThrowIfFailed( HRESULT hr )
{
	if ( FAILED( hr ) )
	{
		throw Exceptions::FailedHResultException( hr );
	}
}
