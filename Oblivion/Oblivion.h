#pragma once

// Windows stuff
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#if defined max
#undef max
#endif
#if defined min
#undef min
#endif

// Com stuff
#include <wrl.h>
using Microsoft::WRL::ComPtr;

// DirectX stuff
#include <d3d12.h>
#include <dxgi1_6.h>
#include "d3dx12.h"
#include <DirectXMath.h>
#include <d3dcompiler.h>


// Standard stuff
#include <exception>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <chrono>
#include <functional>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <optional>
#include <algorithm>
#include <thread>
#include <memory>
#include <cstring>
#include <type_traits>
#include <queue>


// My stuff
#include "Exceptions.h"
#include "ISingletone.h"
#include "Events.h"
#include "CommonMath.h"
#include "Hashers.h"

#define _KB(x) (x * 1024)
#define _MB(x) (x * 1024 * 1024)

#define _64KB _KB(64)
#define _1MB _MB(1)
#define _2MB _MB(2)
#define _4MB _MB(4)
#define _8MB _MB(8)
#define _16MB _MB(16)
#define _32MB _MB(32)
#define _64MB _MB(64)
#define _128MB _MB(128)
#define _256MB _MB(256)

#define STR1(x) #x
#define STR(x) STR1(x)
#define WSTR1(x) L##x
#define WSTR(x) WSTR1(x)
#if DEBUG || _DEBUG
#define NAME_D3D12_OBJECT(x) x->SetName( WSTR(__FILE__ "(" STR(__LINE__) "): " L#x) )
#endif



constexpr const char* APPLICATION_NAME = "Game";
constexpr const char* ENGINE_NAME = "Oblivion";


#pragma region appendToString()

template <typename ... Args>
constexpr auto appendToString( ) -> std::string
{
    return "";
}

template <typename type>
constexpr auto appendToString( type arg ) -> std::string
{
    std::ostringstream stream;
    stream << arg;
    return stream.str( );
}

template <typename type, typename... Args>
constexpr auto appendToString( type A, Args... args ) -> std::string
{
    std::string res1 = appendToString( A );
    std::string res2 = appendToString( args... );
    return res1 + res2;
}

#pragma endregion

namespace Logger
{
    template <typename type, typename... Args>
    constexpr auto DebugPrint( type arg, Args... args )
    {
#if DEBUG || _DEBUG
        OutputDebugStringA( appendToString( arg, args... ).c_str( ) );
#endif
    }
}




